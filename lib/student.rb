
class Student
  attr_reader :first_name, :last_name, :courses, :course_load, :name
  def initialize(first, last)
    @name = first + " " + last
    @first_name = first
    @last_name = last
    @courses = []

  end

  def course_load
    @course_load = Hash.new(0)
    @courses.each do |course|
      @course_load[course.department] += course.credits
    end
    @course_load
  end

  def enroll(course)

    # If the student is already enrolled, raise an error
    if course.students.include?(self)
      return
    else

      # Push the course into student#courses and update the student
      # list for the course
      @courses.push(course)
      course.students.push(self)

    end
  end
end
